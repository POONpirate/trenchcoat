name = "Trenchcoat"
description = [[
A trenchcoat to keep the sand out of your eyes.

Recipe: (Blueprint from killing antlion)
-- Desert goggles
]]

author = "Kelso"
version = "1.0.1"
forumthread = ""
api_version = 10
dst_compatible = true

all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {}

icon_atlas = "images/modicon.xml"
icon = "modicon.tex"

--configuration_options = 
--{
--}