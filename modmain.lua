Assets = {
    Asset("ATLAS", "images/inventoryimages/trenchcoat.xml"),
    Asset("IMAGE", "images/inventoryimages/trenchcoat.tex"),
}

PrefabFiles =
{
    "trenchcoat",
}

local trenchcoat = AddRecipe("trenchcoat", {GLOBAL.Ingredient("deserthat",1)}, GLOBAL.RECIPETABS.DRESS, GLOBAL.TECH.LOST)
trenchcoat.atlas = "images/inventoryimages/trenchcoat.xml"
trenchcoat.image = "trenchcoat.tex"
local trenchcoat_sortkey = GLOBAL.AllRecipes["deserthat"]["sortkey"]
trenchcoat.sortkey = trenchcoat_sortkey + 0.1
GLOBAL.STRINGS.NAMES.TRENCHCOAT = "Trenchcoat"
GLOBAL.STRINGS.RECIPE_DESC.TRENCHCOAT = "Keeps me safe from the sand."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.TRENCHCOAT = "Keeps me safe from the sand."

if GLOBAL.TheNet:GetIsServer() then
	AddPrefabPostInit("antlion", function(inst) inst.components.lootdropper:AddChanceLoot("trenchcoat_blueprint", 1) end)
end
